import { getAssetFromKV, mapRequestToAsset } from '@cloudflare/kv-asset-handler'

// does NOT pass through on exception intentionally
addEventListener('fetch', event => {
  try {
    event.respondWith(handleEvent(event))
  } catch (e) {
    return event.respondWith(
      new Response(e.message || e.toString(), {
        status: 500,
      }),
    )
  }
})

/**
 * handleEvent()
 * Sets up logging.
 * Routes request to Cloudflare KV or Bitwarden API, snarfing settings.
 * Fetches response, applying secure headers.
 * Writes logs.
 */
async function handleEvent(event) {
  // preflight checks including parameter setup and route/cache sensing
  const {
    request
  } = event;
  const rMeth = request.method
  const rUrl = request.url
  const uAgent = request.headers.get("user-agent")
  const rHost = request.headers.get("host")
  const cfRay = request.headers.get("cf-ray")
  const cIP = request.headers.get("cf-connecting-ip")
  const rCf = request.cf
  const requestMetadata = buildMetadataFromHeaders(request.headers)

  const sourceKey = "SED_REPLACE_LOGFLARE_SRC"
  const apiKey = "SED_REPLACE_LOGFLARE_API"
  const WORKER_ID = makeid(6)

  const t1 = Date.now()

  // check incoming request route to determine source and cache settings
  const originalURL = new URL(event.request.url)
  const {api, cache} = getDefaultsFromRoute(originalURL)
  let response = null

  // route to proper source
  if(api) {
    // ref: streaming responses
    // https://developers.cloudflare.com/workers/reference/apis/streams/
    let responseStream = await fetch(event.request)
    let { readable, writable } = new TransformStream()
    responseStream.body.pipeTo(writable)

    // whatever we got (are getting?), set it as response
    response = createResponseWithHeaders(readable, responseStream, cache)
  } else {
    // ref: effectively, an all-static KV-stored website
    // https://github.com/partridge-tech/chris-blog/blob/master/workers-site/
    try {
      let options = {}
      const page = await getAssetFromKV(event, options)
      response = createResponseWithHeaders(page.body, page, cache)
    } catch (e) {
      // we have no 404 page to fetch so... just 500 it for now :pepehands:
      response = new Response(e.message || e.toString(), { status: 500, })
    }
  }

  // push results to Logflare
  // TODO: should this be in a try/catch? should we give responses if logging fails?
  const originTimeMs = Date.now() - t1
  const statusCode = response.status
  const responseMetadata = buildMetadataFromHeaders(response.headers)
  const logEntry = `${rMeth} | ${statusCode} | ${cIP} | ${cfRay} | ${rUrl} | ${uAgent}`

  // TODO: tune the heck out of this to only useful stuff
  const logflareEventBody = {
    source: sourceKey,
    log_entry: logEntry,
    metadata: {
      response: {
        headers: responseMetadata,
        origin_time: originTimeMs,
        status_code: response.status,
      },
      request: {
        url: rUrl,
        method: rMeth,
        headers: requestMetadata,
        cf: rCf,
      },
      logflare_worker: {
        worker_id: WORKER_ID,
      },
    },
  }

  const init = {
    method: "POST",
    headers: {
      "X-API-KEY": apiKey,
      "Content-Type": "application/json",
      "User-Agent": `Cloudflare Worker via ${rHost}`
    },
    body: JSON.stringify(logflareEventBody),
  }

  event.waitUntil(fetch("https://api.logflare.app/logs", init))

  return response
}

/**
 * getDefaultsFromRoute()
 * Interprets path to set API or non-API source and fetches cache settings.
 */
function getDefaultsFromRoute(originalURL) {
  // defaults
  let api = false // false == not API, ergo serve content from KV only
  let cache = "" // empty == do not send a cache control header

  const path = originalURL.pathname.toLowerCase()

  // TODO: remove admin?
  const apiRoutes = [
    "api", "admin", "alive", "bwrs", "identity", "icons", "notifications", "attachments"
  ]

  for(const route of apiRoutes) {
    if(path.indexOf(route) == 1) {
      api = true // true == is API, pass to source
      break
    }
  }
  
  const disableCacheRoutes = [
    "api", "admin", "alive", "identity", "notifications", "attachments"
  ]

  let assignCache = false
  for(const route of disableCacheRoutes) {
    if(path.indexOf(route) == 1) {
      assignCache = true
      break
    }
  }

  if(assignCache) {
    cache = getDefaultCacheControl(path) // may optionally become non-empty
  }

  return {api: api, cache: cache}
}

/**
 * getDefaultCacheControl()
 * Assumes the extension from the last part of the path, if applicable.
 */
function getDefaultCacheControl(path) {
  const extension = path.split(".").pop()

  switch(extension) {
    /* ONE YEAR CACHE
     * For: fonts
     * Changes: never
     */
    case "ttf":
      return "max-age=31536000"
    case "woff":
      return "max-age=31536000"
    case "woff2":
      return "max-age=31536000"

    /* TWO WEEK CACHE
     * For: js, css
     * Changes: unlikely, due to node use
     */
    case "css":
      return "max-age=1209600"
    case "js":
      return "max-age=1209600"

    /* FOUR DAY CACHE
     * For: photos, icons
     * Changes: likely, but assets are large
     */
    case "eot":
      return "max-age=345600"
    case "ico":
      return "max-age=345600"
    case "jpg":
      return "max-age=345600"
    case "png":
      return "max-age=345600"
    case "svg":
      return "max-age=345600"
    case "webmanifest":
      return "max-age=345600"

    /* TWELVE HOUR CACHE
     * For: json files such as translations
     * Changes: likely
     */
    case "json":
      return "max-age=43200"

    /* NO CACHE
     * For: everything else, ex. txt, xml, html
     */
    default:
      return ""
  }
}

/**
 * createResponseWithHeaders() 
 * Adds browser-side cache directives and security headers to responses.
 */
function createResponseWithHeaders(body, page, cache) {
  // make a header-mutable response
  let response = new Response(body, page)

  // performance headers
  if(cache) {
    response.headers.set("Cache-Control", cache)
  }

  // security headers
  response.headers.set('Feature-Policy', 'accelerometer \'none\'; ambient-light-sensor \'none\'; autoplay \'none\'; camera \'none\'; encrypted-media \'none\'; fullscreen \'none\'; geolocation \'none\'; gyroscope \'none\'; magnetometer \'none\'; microphone \'none\'; midi \'none\'; payment \'none\'; picture-in-picture \'none\'; sync-xhr \'self\' https://haveibeenpwned.com https://twofactorauth.org; usb \'none\'; vr \'none\'')
  response.headers.set('X-XSS-Protection', '1; mode=block')
  response.headers.set('X-Content-Type-Options', 'nosniff')
  response.headers.set('X-Frame-Options', 'DENY')
  response.headers.set('Content-Security-Policy', 'default-src https://SED_REPLACE_FQDN:443; img-src data: https://SED_REPLACE_FQDN:443; style-src \'unsafe-inline\' https://SED_REPLACE_FQDN:443; frame-ancestors \'self\' chrome-extension://nngceckbapebfimnlniiiahkandclblb moz-extension://*;')
  response.headers.set('Referrer-Policy', 'same-origin')

  return response
}

/**
 * makeid()
 * Logflare-required
 */
const makeid = length => {
  let text = ""
  const possible = "ABCDEFGHIJKLMNPQRSTUVWXYZ0123456789"
  for (let i = 0; i < length; i += 1) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return text
}

/**
 * buildMetadataFromHeaders()
 * Logflare-required
 */
const buildMetadataFromHeaders = headers => {
  const responseMetadata = {}
  Array.from(headers).forEach(([key, value]) => {
    responseMetadata[key.replace(/-/g, "_")] = value
  })
  return responseMetadata
}