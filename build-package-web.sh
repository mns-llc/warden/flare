#!/bin/bash

echo "--- fetching bitwarden/web and dani-garcia/bw_web_builds"
git clone https://gitlab.com/securitygolf/warden/web.git web
git clone https://gitlab.com/securitygolf/warden/web-patch.git patch

if [[ -z "$VAULT_VERSION" ]]; then
  VAULT_VERSION="v2.15.1"
fi

echo "--- checking out version $VAULT_VERSION"
cd web
git checkout $VAULT_VERSION

echo "--- updating submodules"
git submodule update --recursive --init

echo "--- applying most relevant patch"
if [ -f "../patch/patches/$VAULT_VERSION.patch" ]; then
  PATCH_NAME="$VAULT_VERSION.patch"
else
  echo "!!! patch file not found, using latest"
  # If not, use the latest one
  PATCH_NAME="$(find ../patch -printf "%f\\n" | sort -V | tail -n1)"
fi

echo "--- patching with $PATCH_NAME"
git apply "../patch/patches/$PATCH_NAME"

echo "--- building bitwarden/web"
npm run sub:init
npm install
npm run dist

if [[ -z "$DEBUG" ]]; then
  echo "--- $DEBUG not set, deleting .map files"
  find build -name "*.map" -delete
fi

echo "--- moving built files to ../bin and saving artifacts"
mv build ../bin