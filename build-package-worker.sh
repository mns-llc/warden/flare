#!/bin/bash
if [ ! -d "./bin" ]; then
  echo "!!! ./bin is missing, did web build successfully? failing deploy"
  exit 1
fi

if [[ -z "$1" ]]; then
  echo "!!! no deployment type provided, failing deploy"
  exit 1
fi

if [[ "$1" == "STAGING" ]]; then
  echo "--- setting STAGING values"
  VAULT_FQDN="staging-vault.security.golf"
  LOGFLARE_SRC=$STAGING_LF_SRC
  LOGFLARE_API=$STAGING_LF_API
elif [[ "$1" == "PRODUCTION" ]]; then
  echo "--- setting PRODUCTION values"
  VAULT_FQDN="vault.security.golf"
  LOGFLARE_SRC=$PRODUCTION_LF_SRC
  LOGFLARE_API=$PRODUCTION_LF_API
else
  echo "!!! not a valid deployment type, failing deploy"
  exit 1
fi

echo "--- modifying source files according to deployment parameters"
sed -i -e "s/SED_REPLACE_FQDN/$VAULT_FQDN/g" worker/index.js
sed -i -e "s/SED_REPLACE_LOGFLARE_SRC/$LOGFLARE_SRC/g" worker/index.js
sed -i -e "s/SED_REPLACE_LOGFLARE_API/$LOGFLARE_API/g" worker/index.js

echo "--- installing worker dependencies"
cd worker
npm install
cd ..